#include "header.h"
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static void unesi_zadatak(FILE *datoteka, Zadatak *zadatak) {
    if (datoteka == NULL || zadatak == NULL) {
        perror("Greska: neispravni parametri za unos zadatka.\n");
        return;
    }

    printf("Unesite zadatak: ");
    scanf(" %[^\n]", zadatak->zadatak);

    int ispravan_datum = 0;
    while (!ispravan_datum) {
        printf("Unesite datum (DD.MM.GGGG): ");
        scanf(" %[^\n]", zadatak->datum);
        ispravan_datum = provjeri_datum(zadatak->datum);
        if (!ispravan_datum) {
            printf("Neispravan format datuma! Pokusajte ponovno.\n");
        }
    }

    printf("Unesite kategoriju: ");
    scanf(" %[^\n]", zadatak->kategorija);

    zadatak->zavrsen = 0;

    fseek(datoteka, 0, SEEK_END);
    fprintf(datoteka, "%s|%s|%d|%s\n", zadatak->datum, zadatak->zadatak, zadatak->zavrsen, zadatak->kategorija);
}

static int provjeri_datum(const char *datum) {
    if (strlen(datum) != 10) {
        return 0;
    }

    for (int i = 0; i < 10; i++) {
        if (i == 2 || i == 5) {
            if (datum[i] != '.') {
                return 0;
            }
        } else {
            if (!isdigit(datum[i])) {
                return 0;
            }
        }
    }

    int dan = atoi(&datum[0]);
    int mjesec = atoi(&datum[3]);
    int godina = atoi(&datum[6]);

    if (dan < 1 || dan > 31 || mjesec < 1 || mjesec > 12) {
        return 0;
    }

    if ((mjesec == 4 || mjesec == 6 || mjesec == 9 || mjesec == 11) && dan > 30) {
        return 0;
    }
    if (mjesec == 2) {
        int prijestupna = (godina % 4 == 0 && (godina % 100 != 0 || godina % 400 == 0));
        if (dan > 29 || (dan == 29 && !prijestupna)) {
            return 0;
        }
    }

    return 1;
}


static void ispisi_zadatak(Zadatak zadatak) {
    printf("%s - %s [%s] %s\n", zadatak.datum, zadatak.zadatak, zadatak.kategorija, zadatak.zavrsen ? "(zavrseno)" : "(nezavrseno)");
}

static int ucitaj_zadatke(FILE *datoteka, Zadatak **zadaci, int *broj_zadataka) {
    if (datoteka == NULL || zadaci == NULL || broj_zadataka == NULL) {
        perror("Greska: neispravni parametri za ucitavanje zadataka.\n");
        return -1;
    }

    fseek(datoteka, 0, SEEK_SET);

    char buffer[256];
    while (fgets(buffer, sizeof(buffer), datoteka)) {
        *zadaci = realloc(*zadaci, (*broj_zadataka + 1) * sizeof(Zadatak));
        if (*zadaci == NULL) {
            perror("Greska pri alokaciji memorije!\n");
            return -1;
        }

        sscanf(buffer, "%[^|]|%[^|]|%d|%[^\n]", (*zadaci)[*broj_zadataka].datum, (*zadaci)[*broj_zadataka].zadatak, &(*zadaci)[*broj_zadataka].zavrsen, (*zadaci)[*broj_zadataka].kategorija);

        (*broj_zadataka)++;
    }

    return 0;
}

static void ispisi_zadatke(Zadatak *zadaci, int broj_zadataka, int zavrseni) {
    if (zadaci == 0) {
        printf("Greska: nema zadataka za ispis.\n");
        return;
    }

    int redni_broj = 1;
    for (int i = 0; i < broj_zadataka; i++) {
        if (zadaci[i].zavrsen == zavrseni) {
            printf("%d. ", redni_broj++);
            ispisi_zadatak(zadaci[i]);
        }
    }
}

static void ispisi_sve_zadatke(Zadatak *zadaci, int broj_zadataka, int trenutni_indeks) {

    if (broj_zadataka == 0) {
        printf("Nema zadataka za ispis.\n");
        return;
    }

    if (trenutni_indeks == broj_zadataka) {
        return;
    }
    
    printf("%d. ", trenutni_indeks + 1);
    ispisi_zadatak(zadaci[trenutni_indeks]);

    ispisi_sve_zadatke(zadaci, broj_zadataka, trenutni_indeks +1);
}

static void oznaci_kao_zavrseno(FILE *datoteka, Zadatak *zadaci, int broj_zadataka) {
    if (datoteka == NULL || zadaci == NULL) {
        perror("Greska: neispravni parametri za oznacavanje zadataka.\n");
        return;
    }

    int indeks;
    printf("Unesite indeks zadatka koji zelite oznaciti kao zavrsen: ");
    scanf("%d", &indeks);

    if (indeks < 1 || indeks > broj_zadataka) {
        printf("Neispravan indeks!\n");
        return;
    }

    zadaci[indeks - 1].zavrsen = 1;

    FILE *privremena_datoteka = fopen("temp.txt", "w");
    if (privremena_datoteka == NULL) {
        perror("Greska pri otvaranju privremene datoteke!\n");
        return;
    }

    for (int i = 0; i < broj_zadataka; i++) {
        fprintf(privremena_datoteka, "%s|%s|%d|%s\n", zadaci[i].datum, zadaci[i].zadatak, zadaci[i].zavrsen, zadaci[i].kategorija);
    }

    fclose(privremena_datoteka);
    fclose(datoteka);

    remove("todo_lista.txt");
    rename("temp.txt", "todo_lista.txt");

    datoteka = fopen("todo_lista.txt", "a+");
    if (datoteka == NULL) {
        perror("Greska pri ponovnom otvaranju originalne datoteke!\n");
    }
}

static void obrisi_odredeni_zadatak(FILE *datoteka, Zadatak **zadaci, int *broj_zadataka) {
    if (datoteka == NULL || zadaci == NULL || *zadaci == NULL || broj_zadataka == NULL) {
        perror("Greska: neispravni parametri za brisanje zadatka.\n");
        return;
    }

    int indeks;
    printf("Unesite indeks zadatka koji zelite obrisati: ");
    scanf("%d", &indeks);

    if (indeks < 1 || indeks > *broj_zadataka) {
        printf("Neispravan indeks!\n");
        return;
    }

    for (int i = indeks - 1; i < *broj_zadataka - 1; i++) {
        (*zadaci)[i] = (*zadaci)[i + 1];
    }

    (*zadaci) = realloc(*zadaci, (*broj_zadataka - 1) * sizeof(Zadatak));
    (*broj_zadataka)--;

    FILE *privremena_datoteka = fopen("temp.txt", "w");
    if (privremena_datoteka == NULL) {
        perror("Greska pri otvaranju privremene datoteke!\n");
        return;
    }

    for (int i = 0; i < *broj_zadataka; i++) {
        fprintf(privremena_datoteka, "%s|%s|%d|%s\n", (*zadaci)[i].datum, (*zadaci)[i].zadatak, (*zadaci)[i].zavrsen, (*zadaci)[i].kategorija);
    }

    fclose(privremena_datoteka);
    fclose(datoteka);

    remove("todo_lista.txt");
    rename("temp.txt", "todo_lista.txt");

    datoteka = fopen("todo_lista.txt", "a+");
    if (datoteka == NULL) {
        perror("Greska pri ponovnom otvaranju originalne datoteke!\n");
    }
}

static void obrisi_sve_zadatke(FILE *datoteka, Zadatak **zadaci, int *broj_zadataka) {
    if (datoteka == NULL || zadaci == NULL || *zadaci == NULL || broj_zadataka == NULL) {
        printf("Greska: neispravni parametri za brisanje zadataka.\n");
        return;
    }

    free(*zadaci);
    *zadaci = NULL;
    *broj_zadataka = 0;

    fclose(datoteka);
    datoteka = fopen("todo_lista.txt", "w");
    if (datoteka == NULL) {
        perror("Greska pri otvaranju datoteke!\n");
    }
}

static void sortiraj_zadatke(Zadatak *zadaci, int broj_zadataka) {
    if (zadaci == NULL) {
        printf("Greska: nema zadataka za sortiranje.\n");
        return;
    }

    for (int i = 0; i < broj_zadataka - 1; i++) {
        for (int j = 0; j < broj_zadataka - i - 1; j++) {
            if (strcmp(zadaci[j].kategorija, zadaci[j + 1].kategorija) > 0) {
                zamijeni_zadatke(&zadaci[j], &zadaci[j + 1]);
            }
        }
    }
}

static inline void zamijeni_zadatke(Zadatak *a, Zadatak *b) {
    Zadatak temp = *a;
    *a = *b;
    *b = temp;
}

static void promijeni_ime_datoteke(char *ime_datoteke, FILE **datoteka) {
    printf("Unesite novo ime datoteke: ");
    scanf(" %[^\n]", ime_datoteke);
    
    fclose(*datoteka);

    *datoteka = fopen(ime_datoteke, "a+");
    if (*datoteka == NULL) {
        printf("Greska pri otvaranju datoteke!\n");
        exit(1);
    }
}