#ifndef HEADER_H
#define HEADER_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_ZADACI 100

typedef struct {
    char zadatak[MAX_ZADACI];
    char datum[20];
    int zavrsen;
    char kategorija[256];
} Zadatak;

extern int globalni_broj_zadataka;

typedef void (*unos_zadatka_func)(FILE *, Zadatak *);
typedef int (*ucitaj_zadatke_func)(FILE *, Zadatak **, int *);
typedef void (*ispisi_zadatke_func)(Zadatak *, int, int);

static void unesi_zadatak(FILE *datoteka, Zadatak *zadatak);
static int ucitaj_zadatke(FILE *datoteka, Zadatak **zadaci, int *broj_zadataka);
static void ispisi_zadatak(Zadatak zadatak);
static void ispisi_zadatke(Zadatak *zadaci, int broj_zadataka, int zavrseni);
static void ispisi_sve_zadatke(Zadatak *zadaci, int broj_zadataka, int trenutni_indeks);
static void oznaci_kao_zavrseno(FILE *datoteka, Zadatak *zadaci, int broj_zadataka);
static void obrisi_odredeni_zadatak(FILE *datoteka, Zadatak **zadaci, int *broj_zadataka);
static void obrisi_sve_zadatke(FILE *datoteka, Zadatak **zadaci, int *broj_zadataka);
static int provjeri_datum(const char *datum);
static void sortiraj_zadatke(Zadatak *zadaci, int broj_zadataka);
static inline void zamijeni_zadatke(Zadatak *a, Zadatak *b);
static void promijeni_ime_datoteke(char *ime_datoteke, FILE **datoteka);

typedef enum {
    UNOS_ZADATKA = 1,
    ISPIS_ZADATAKA,
    OZNAKA_ZADATKA,
    BRISANJE_ZADATAKA,
    PROMJENA_IMENA_DATOTEKE,
    IZLAZ
} Opcije;

typedef enum {
    ISPIS_NEZAVRSENIH = 1,
    ISPIS_ZAVRSENIH,
    ISPIS_SVIH
} IspisOpcije;

typedef enum {
    BRISANJE_ODREDJENOG = 1,
    BRISANJE_SVIH
} BrisanjeOpcije;

#endif
