#include "header.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "functions.c"

int globalni_broj_zadataka = 0;

void ocisti_ulaz() {
    int c;
    while ((c = getchar()) != '\n' && c != EOF) {}
}

int main() {
    FILE *datoteka;
    char ime_datoteke[50] = "todo_lista.txt";
    int odabir;
    Zadatak *zadaci = NULL;

    unos_zadatka_func unos_zadatka = unesi_zadatak;
    ucitaj_zadatke_func ucitaj_zadatkee = ucitaj_zadatke;
    ispisi_zadatke_func ispisi_sve_zadatkee = ispisi_sve_zadatke;

    datoteka = fopen(ime_datoteke, "a+");
    if (datoteka == NULL) {
        perror("Greska pri otvaranju datoteke!");
        return 1;
    }

    if (ucitaj_zadatke(datoteka, &zadaci, &globalni_broj_zadataka) == -1) {
        fclose(datoteka);
        return 1;
    }

    do {
        printf("\n1. Unos zadatka\n");
        printf("2. Ispis zadataka\n");
        printf("3. Oznaci zadatak kao zavrsen\n");
        printf("4. Brisanje zadataka\n");
        printf("5. Promijeni ime datoteke\n");
        printf("6. Izlaz\n");
        printf("Odaberite opciju: ");
        
        if (scanf("%d", &odabir) != 1) {
            printf("Neispravan unos. Molimo unesite broj izmedju 1 i 6.\n");
            ocisti_ulaz();
            continue;
        }

        switch (odabir) {
            case UNOS_ZADATKA:
                zadaci = realloc(zadaci, (globalni_broj_zadataka + 1) * sizeof(Zadatak));
                if (zadaci == NULL) {
                    perror("Greska pri alokaciji memorije!\n");
                    fclose(datoteka);
                    return 1;
                }
                unos_zadatka(datoteka, &zadaci[globalni_broj_zadataka]);
                globalni_broj_zadataka++;
                sortiraj_zadatke(zadaci, globalni_broj_zadataka);
                break;
            case ISPIS_ZADATAKA: {
                int pododabir;
                printf("\n1. Ispis nezavrsenih zadataka\n");
                printf("2. Ispis zavrsenih zadataka\n");
                printf("3. Ispis svih zadataka\n");
                printf("Odaberite opciju: ");
                
                if (scanf("%d", &pododabir) != 1) {
                    printf("Neispravan unos. Molimo unesite broj izmedju 1 i 3.\n");
                    ocisti_ulaz();
                    continue;
                }

                switch (pododabir) {
                    case ISPIS_NEZAVRSENIH: {
                        int nezavrseni_zadaci = 0;
                        for (int i = 0; i < globalni_broj_zadataka; i++) {
                            if (!zadaci[i].zavrsen) {
                                nezavrseni_zadaci++;
                            }
                        }
                        if (nezavrseni_zadaci == 0) {
                            printf("\nNema nezavrsenih zadataka.\n");
                        } else {
                            printf("\nNezavrseni zadaci:\n");
                            ispisi_zadatke(zadaci, globalni_broj_zadataka, 0);
                        }
                        break;
                    }
                    case ISPIS_ZAVRSENIH: {
                        int zavrseni_zadaci = 0;
                        for (int i = 0; i < globalni_broj_zadataka; i++) {
                            if (zadaci[i].zavrsen) {
                                zavrseni_zadaci++;
                            }
                        }
                        if (zavrseni_zadaci == 0) {
                            printf("\nNema zavrsenih zadataka.\n");
                        } else {
                            printf("\nZavrseni zadaci:\n");
                            ispisi_zadatke(zadaci, globalni_broj_zadataka, 1);
                        }
                        break;
                    }
                    case ISPIS_SVIH:
                        printf("\nSvi zadaci:\n");
                        ispisi_sve_zadatkee(zadaci, globalni_broj_zadataka, 0);
                        break;
                    default:
                        printf("Nepoznata opcija!\n");
                        break;
                }
                break;
            }
            case OZNAKA_ZADATKA:
                if (globalni_broj_zadataka > 0) {
                    ispisi_sve_zadatkee(zadaci, globalni_broj_zadataka, 0);
                    oznaci_kao_zavrseno(datoteka, zadaci, globalni_broj_zadataka);
                } else {
                    printf("Nema zadataka za oznaciti kao zavrsene.\n");
                }
                break;
            case BRISANJE_ZADATAKA: {
                if (globalni_broj_zadataka > 0) {
                    int pododabir;
                    printf("\n1. Obrisi odredeni zadatak\n");
                    printf("2. Obrisi sve zadatke\n");
                    printf("Odaberite opciju: ");
                    
                    if (scanf("%d", &pododabir) != 1) {
                        printf("Neispravan unos. Molimo unesite broj izmedju 1 i 2.\n");
                        ocisti_ulaz();
                        continue;
                    }

                    switch (pododabir) {
                        case BRISANJE_ODREDJENOG:
                            ispisi_sve_zadatkee(zadaci, globalni_broj_zadataka, 0);
                            obrisi_odredeni_zadatak(datoteka, &zadaci, &globalni_broj_zadataka);
                            break;
                        case BRISANJE_SVIH:
                            obrisi_sve_zadatke(datoteka, &zadaci, &globalni_broj_zadataka);
                            break;
                        default:
                            printf("Nepoznata opcija!\n");
                            break;
                    }
                } else {
                    printf("Nema zadataka za brisanje.\n");
                }
                break;
            }
            case PROMJENA_IMENA_DATOTEKE:
                promijeni_ime_datoteke(ime_datoteke, &datoteka);
                break;
            case IZLAZ:
                printf("Izlaz iz programa.\n");
                break;
            default:
                printf("Nepoznata opcija!\n");
                break;
        }
    } while (odabir != IZLAZ);

    free(zadaci);
    fclose(datoteka);

    return 0;
}
